# Carpooling-Team-11

## Overview

Welcome to the Carpooling Web Application! This application provides a platform for users to share rides, reduce their carbon footprint, and build a community of travelers. Whether you're a driver looking to share your journey or a passenger seeking a convenient and eco-friendly way to travel, Carpooling has you covered. 

## Features

- **User Profiles:**
    - Register with a unique username, password, and complete your profile with essential details.

 
- **Profile Management:**
    - Users can update their personal details and change password.

- **Create Travels:**
    - As a driver, create rides by specifying starting and ending points, departure time, and available spots.

- **Search and Join Rides:**
    - Passengers can browse and apply for available rides based on their preferences.

- **Passenger Approval:**
    - Drivers can review passenger applications and choose who joins their ride.

- **Feedback and Ratings:**
    - Passengers and drivers can leave feedback and ratings for each other after completing a ride.

- **Admin Panel:**
    - Admin users can manage users, rides.
    - Ability to block and unblock users.


- **API Documentation with Sample Requests:**
    - Our API documentation is available via Swagger UI. It provides an intuitive interface for understanding our API endpoints and also showcases sample requests for each operation.
    - https://localhost:7091/api/swagger

## Technologies Used

**This project uses several open-source technologies, including:**
* ASP.NET Core 6.0
* ASP.NET Core 6.0 Web API
* ASP.Net MVC
* Swagger
* C#
* HTML 5
* CSS 3
* Entity Framework
* SQL Database
* Razor Pages
* Bootstrap
* Javascript
* Moq



## Installation

Follow these steps to set up and run the application:

1. **Download the App**
    - Visit the project's repository and clone or download the app source code to your local machine.

2. **Configure Database Connection**
    - Locate the appsettings.json files in project within the solution.
Modify the "DefaultConnection" string in file to include your database server and database name details. Utilize the following format as a reference: 

![Connection-string](/uploads/0317495fc109467a2c883dbf1e56b05d/Connection-string.png)

3. **Run the Application**
    - Open the project in your preferred IDE.
    - Build and initiate the application. The database will automatically be created during the initial run based on the connection details provided in the second step.

- **Home Page**
 ![Home-Page](/uploads/88421d390a841dc1b7cd619b3bbd71a7/Home-Page.png)

- **Register Page**
 ![Register-Page](/uploads/31e5d833c6fe1b08c3bb49217e013cfa/Register-Page.png)

- **Login Page**
 ![Login-Page](/uploads/5b1df6b82356a40d6ff79752cff042fb/Login-Page.png)

- **Profile Page**
  ![Profile_Page](/uploads/f82e003d02c0edf71d2d92db87032d3d/Profile_Page.png)

- **Travel Search Page**
   ![TravelSearch-Page](/uploads/9e979dcb2a909ab131ff7e7b5f191bd5/TravelSearch-Page.png)

- **Create Car Page**
 ![CreateCar-Page](/uploads/f2c479bc780ca04a2e965b99cec370d7/CreateCar-Page.png)

- **My Cars Page**
 ![MyCars-Page](/uploads/96fe6d249a34946298c3c3da27e104f3/MyCars-Page.png)

- **Create Travel Page**
 ![CreateTravel-Page](/uploads/c11db990f6b488e231c7450fdac0ccfc/CreateTravel-Page.png)

- **My Travel Page**
 ![MyTravel-Page](/uploads/a5d1af132519318a8d73860da5186984/MyTravel-Page.png)

- **Driver Info Page**
 ![DriverInfo-Page](/uploads/c4c66bbedeb302e4f840825fc752d250/DriverInfo-Page.png)

 - **Travel Details Page**
  ![TravelDetails-Page](/uploads/07ff73f94761228fc654527b73a99225/TravelDetails-Page.png)

- **Personal Driver Info Page**
 ![PersonalDriverInfo-Page](/uploads/9d0398ddd678ab33d38d9d6dc0386146/PersonalDriverInfo-Page.png)

- **Admin Panel Page**
 ![AdminPanel-Page](/uploads/9a2736b82500201a6d72b84c427d1057/AdminPanel-Page.png)

- **Database Diagram**
 ![Database-Diagram](/uploads/d2cf99353dc7f1976e15830cc38e9a3c/Database-Diagram.png)


- **ASP.NET Framework Web Application**
This documentation explains the main architectural principles of our ASP.NET MVC 6 project, focusing on solution structure and the purpose of each project within the solution. Adhering to these principles allows us to maintain well-structured code and organized workflows.

- **Solution Structure**
The solution is composed of the following projects:

* CarPooling.API/MVC
* CarPooling.Data
* CarPooling.BusinessLayer

**CarPooling**
This project contains our API's and MVC's controllers, which have the required services injected. The services for the API/MVC are configured in the Program.cs file. The database connection string is set in the appsettings.json file, while the connection itself is established in the Program.cs file. For client-side interactions(MVC part), containing all HTML, CSS, and JS files within the wwwroot folder. It follows the Model-View-Controller (MVC) pattern.

- **Required NuGet packages:** AutoMapper, FluentValidation, FontAwesome, Identity EF, Identity.UI, NewtonsofJson, EntityFramework.SqlServer, Tools, and Design, Web.CodeGeneration.Design, Moq, Swager.

**CarPooling.Data**
The CarPooling.Data project is responsible for defining our database tables via the CarPoolingDbContext class. This class injects DbContextOptions for database operations. Configurations for foreign keys and other database settings are kept in a separate file within the project. Any repositories within our architecture inject the CarPoolingDbContext class in their constructors.
 - **Required NuGet packages:** AutoMapper, Http.Abstractions, Identity EF, EntityFramework.SqlServer, Tools, and Design, Moq.

**CarPooling.BusinessLayer**
Carpooling.BusinessLayer includes folders for interface definitions, service classes implementing business logic. The constructors for services have the necessary repositories injected.
 - **Required NuGet packages:** AutoMapper, FluentValidation, Moq.

 ## References between Projects
 
 * Carpooling: References Carpooling.Data and Carpooling.BusinessLayer.
 * Carpooling.Data: Standalone with no references to other projects.
 * Carpooling.BusinessLayer: References Carpooling.Data.

## Integration Tests Coverage Report 🚀

**📊 Coverage Breakdown**

Services
✅ 62.3% test coverage.

## License
This project is licensed under the MIT [LICENSE](https://gitlab.com/bobi_91/carpool-project/-/blob/main/LICENSE?ref_type=heads) - see the LICENSE file for details.

## Conclusion

Thank you for choosing Carpooling Web Application! Join us in building a greener, more connected world. 🌍🚗
